#!/bin/bash

declare -r EXCLUDES=$(dirname $BASH_SOURCE)/exclude.txt
declare -r REPO_ROOT=$(dirname $BASH_SOURCE)


if [ "$1" = "prod" ]; then
	declare -r TARGET_DIR=msjmcadmin@10.77.76.181:/var/www/html
else
	echo "Please specify one of [prod] as deploy target"
	exit
fi

if [ "$2" = "go" ];then
        rsync -avO --itemize-changes --delete --exclude-from $EXCLUDES $REPO_ROOT $TARGET_DIR

else
        rsync -avO --itemize-changes --delete --dry-run --exclude-from  $EXCLUDES $REPO_ROOT $TARGET_DIR
fi
