-- MySQL dump 10.13  Distrib 5.7.25, for osx10.14 (x86_64)
--
-- Host: localhost    Database: outpatient
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) NOT NULL,
  `schedule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patient_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `chamber_id` int(11) NOT NULL,
  `is_visited` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`appointment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (1,1573322400,'9:00 AM - 11:00 AM',1,1,1,0),(2,1573495200,'12:00 PM - 6:00 PM',2,1,3,0),(3,1573686000,'6:00 PM - 12:00 AM',3,2,3,0),(4,1573513200,'12:00 PM - 6:00 PM',3,2,3,0),(5,1573772400,'6:00 PM - 10:30 PM',3,1,3,0),(6,1573513200,'12:00 PM - 6:00 PM',3,1,3,0),(7,1573513200,'12:00 PM - 6:00 PM',4,2,3,1),(10,1573945200,'9:00 AM - 11:00 AM',4,1,1,0),(11,1573945200,'9:00 AM - 11:00 AM',5,1,1,0),(12,1574118000,'11:00 AM - 11:30 AM',5,1,1,0),(13,1574118000,'11:00 AM - 11:30 AM',6,6,1,0),(14,1578092400,NULL,7,1,4,1),(15,1578524400,'6:00 PM - 12:00 AM',2,4,3,0),(16,1578956400,'12:00 PM - 6:00 PM',3,4,3,0),(17,1578524400,'11:30 AM - 12:00 PM',4,1,1,0);
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chamber`
--

DROP TABLE IF EXISTS `chamber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chamber` (
  `chamber_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `schedule` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`chamber_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chamber`
--

LOCK TABLES `chamber` WRITE;
/*!40000 ALTER TABLE `chamber` DISABLE KEYS */;
INSERT INTO `chamber` VALUES (5,'Pharmacy','Central MSJMC','','');
/*!40000 ALTER TABLE `chamber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('aeka9flv4gckgubgmeckgk2seq8kq26k','::1',1586348049,_binary '__ci_last_regenerate|i:1586348049;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:5:\"staff\";login_user_id|s:2:\"10\";name|s:7:\"Testing\";user_type|s:1:\"2\";phone|s:8:\"12345678\";user_email|s:14:\"test@gmail.com\";chamber_id|s:1:\"5\";'),('nqfkkib0pit2n9ejtein7p234b3q9bln','::1',1586349085,_binary '__ci_last_regenerate|i:1586349085;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:5:\"staff\";login_user_id|s:2:\"10\";name|s:7:\"Testing\";user_type|s:1:\"2\";phone|s:8:\"12345678\";user_email|s:14:\"test@gmail.com\";chamber_id|s:1:\"5\";'),('taclje0ghqqmpfafahm64fkm8bh42r78','::1',1586349421,_binary '__ci_last_regenerate|i:1586349421;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:6:\"doctor\";login_user_id|s:1:\"3\";name|s:13:\"IT Department\";user_type|s:1:\"1\";phone|s:11:\"12684802700\";user_email|s:12:\"it@msjmc.com\";chamber_id|s:1:\"5\";doctor_login|s:1:\"1\";'),('uetl4feu49bkondkcpb4m46qle1caqh4','::1',1586350295,_binary '__ci_last_regenerate|i:1586350295;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:6:\"doctor\";login_user_id|s:1:\"3\";name|s:13:\"IT Department\";user_type|s:1:\"1\";phone|s:11:\"12684802700\";user_email|s:12:\"it@msjmc.com\";chamber_id|s:1:\"5\";doctor_login|s:1:\"1\";'),('nv2dltq3l0kqiqbkbq2vg4ldclus1n25','::1',1586350267,_binary '__ci_last_regenerate|i:1586350267;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:5:\"staff\";login_user_id|s:2:\"10\";name|s:7:\"Testing\";user_type|s:1:\"2\";phone|s:8:\"12345678\";user_email|s:14:\"test@gmail.com\";chamber_id|s:1:\"5\";'),('lb5ribaddlvc3558h3m9kerq7avlktsg','::1',1586351745,_binary '__ci_last_regenerate|i:1586351745;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:6:\"doctor\";login_user_id|s:1:\"3\";name|s:13:\"IT Department\";user_type|s:1:\"1\";phone|s:11:\"12684802700\";user_email|s:12:\"it@msjmc.com\";chamber_id|s:1:\"5\";doctor_login|s:1:\"1\";success_message|s:30:\"Staff Was Created Successfully\";__ci_vars|a:1:{s:15:\"success_message\";s:3:\"old\";}'),('uhqdimi41p58f5nc629plktjc1bmh71q','::1',1586351024,_binary '__ci_last_regenerate|i:1586351024;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:5:\"staff\";login_user_id|s:2:\"11\";name|s:11:\"Dane Abbott\";user_type|s:1:\"2\";phone|s:4:\"1234\";user_email|s:21:\"dane.abbott@msjmc.org\";chamber_id|s:1:\"5\";'),('goo1p3qnmta578j6mgls3o8ggdrvnaq9','::1',1586351111,_binary '__ci_last_regenerate|i:1586351024;current_language|s:7:\"english\";staff_login|s:1:\"1\";login_type|s:5:\"staff\";login_user_id|s:2:\"11\";name|s:11:\"Dane Abbott\";user_type|s:1:\"2\";phone|s:4:\"1234\";user_email|s:21:\"dane.abbott@msjmc.org\";chamber_id|s:1:\"5\";'),('vm8bdhii8hcesotpugbik0pop152npfg','::1',1586352092,_binary '__ci_last_regenerate|i:1586352092;current_language|s:7:\"english\";'),('4npia0en2kp9r0ik09m4a7r3jubspsba','::1',1586352102,_binary '__ci_last_regenerate|i:1586352092;current_language|s:7:\"english\";doctor_login|s:1:\"1\";login_type|s:6:\"doctor\";login_user_id|s:1:\"3\";name|s:13:\"IT Department\";user_type|s:1:\"1\";phone|s:11:\"12684802700\";user_email|s:12:\"it@msjmc.org\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fend_blog`
--

DROP TABLE IF EXISTS `fend_blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fend_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=armscii8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fend_blog`
--

LOCK TABLES `fend_blog` WRITE;
/*!40000 ALTER TABLE `fend_blog` DISABLE KEYS */;
INSERT INTO `fend_blog` VALUES (1,'Koala - Copy.jpg','Scary Thing That You Don’t Get ',1574982000,'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.\r\nFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.\r\nFar far away, behin'),(2,'Desert.jpg','Scary Thing ',1574982000,'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.'),(3,'index.jpg','Caring for military veterans ',1578265200,'Although the United States government designates a single day (Veterans Day) to specifically honor persons with a history of military service, family physicians provide care to veterans all 365 days of the year. A review article and editorial in the Novem');
/*!40000 ALTER TABLE `fend_blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fend_promocontent`
--

DROP TABLE IF EXISTS `fend_promocontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fend_promocontent` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `font_awesome_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `promo_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `promo_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=armscii8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fend_promocontent`
--

LOCK TABLES `fend_promocontent` WRITE;
/*!40000 ALTER TABLE `fend_promocontent` DISABLE KEYS */;
INSERT INTO `fend_promocontent` VALUES (8,'fas fa-car','Emergency Care','loren epsum sit ament'),(9,'fas fa-adjust','Good Care','basaSA  AHKHASDHAS AS\r\nASDJLASJDAS'),(10,'fab fa-amazon-pay','CURRENT SUPPORT','ZDASA ASDASDASDSAD');
/*!40000 ALTER TABLE `fend_promocontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fend_qualification`
--

DROP TABLE IF EXISTS `fend_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fend_qualification` (
  `qualification_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `qualification_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`qualification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=armscii8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fend_qualification`
--

LOCK TABLES `fend_qualification` WRITE;
/*!40000 ALTER TABLE `fend_qualification` DISABLE KEYS */;
INSERT INTO `fend_qualification` VALUES (1,'about-2.jpg','Medical specialty concerned with the care ','On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word');
/*!40000 ALTER TABLE `fend_qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fend_slidercontent`
--

DROP TABLE IF EXISTS `fend_slidercontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fend_slidercontent` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `short_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slider_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=armscii8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fend_slidercontent`
--

LOCK TABLES `fend_slidercontent` WRITE;
/*!40000 ALTER TABLE `fend_slidercontent` DISABLE KEYS */;
INSERT INTO `fend_slidercontent` VALUES (18,'Our support','make you happy','loren epsum sit ament','Lighthouse.jpg'),(19,'we care','client staisfaction','adasdads','Chrysanthemum.jpg'),(20,'Our moto','Quick Response','loren epsum sit ament','Jellyfish.jpg');
/*!40000 ALTER TABLE `fend_slidercontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fend_testimonial`
--

DROP TABLE IF EXISTS `fend_testimonial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fend_testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `testimonial_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=armscii8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fend_testimonial`
--

LOCK TABLES `fend_testimonial` WRITE;
/*!40000 ALTER TABLE `fend_testimonial` DISABLE KEYS */;
INSERT INTO `fend_testimonial` VALUES (1,'Rodel Golez','Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.','Jellyfish.jpg','0'),(2,'jeson roy','service is good. qualified doctor and staff','Penguins.jpg','0'),(3,'john','good service and helpfull satff','index.jpg','');
/*!40000 ALTER TABLE `fend_testimonial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `patient_id` int(11) NOT NULL DEFAULT '0',
  `appointment_id` int(11) NOT NULL DEFAULT '0',
  `charge` decimal(10,0) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1-paid 0-unpaid',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `chamber_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (1,'ff0320e','yyyygvg',1,0,20,1,1,3,1573322400),(2,'5e376ed','test payment',5,0,200,1,1,1,1574204400);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine` (
  `medicine_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `about` text COLLATE utf8_unicode_ci,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medical_info` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (9,NULL,'Test Patient','123456789',NULL,NULL,'22','female',NULL),(10,NULL,'Jane Doe','2687817129',NULL,NULL,'18','female',NULL),(11,NULL,'Test Test','1 2 3 4 5',NULL,NULL,'22','male',NULL);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescription` (
  `prescription_id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `symptom` text COLLATE utf8_unicode_ci NOT NULL,
  `diagnosis` text COLLATE utf8_unicode_ci NOT NULL,
  `medicine` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'json',
  `test` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'json',
  `patient_id` int(11) NOT NULL,
  `chamber_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `accountability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`prescription_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (20,0,'','','[{\"medicine_name\":\"testonol\",\"note\":\"123455\"},{\"medicine_name\":\"\",\"note\":\"\"}]','[{\"test_name\":\"x-ray\",\"note\":\"123456\"},{\"test_name\":\"\",\"note\":\"\"}]',10,5,1586217600,'Zecare Thomas'),(21,0,'Testing 1 2 3 ','','[{\"medicine_name\":\"Test\",\"note\":\"Test\"},{\"medicine_name\":\"\",\"note\":\"\"}]','[{\"test_name\":\"Test\",\"note\":\"Test\"},{\"test_name\":\"\",\"note\":\"\"}]',11,5,1586304000,'Dane Abbott');
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`settings_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'doctor_name','Zecare Thomas'),(2,'chamber_id','5'),(3,'currency','USD'),(4,'language','english'),(5,'logo','favicon.png'),(6,'doctor_email','doctor@doctorchamber.com');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staffusers`
--

DROP TABLE IF EXISTS `staffusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staffusers` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(3) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  `chamber` int(3) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staffusers`
--

LOCK TABLES `staffusers` WRITE;
/*!40000 ALTER TABLE `staffusers` DISABLE KEYS */;
INSERT INTO `staffusers` VALUES (10,2,'Testing','test@gmail.com','a94a8fe5ccb19ba61c4c0873d391e987982fbbd3','12345678',1586349687,5),(11,2,'Dane Abbott','dane.abbott@msjmc.org','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','1234',1586350483,5);
/*!40000 ALTER TABLE `staffusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(3) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `auth_token` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Zecare Thomas','doctor@example.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','01717055740',1586281826,''),(3,1,'IT Department','it@msjmc.org','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','12684802700',1586352092,'');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-08  9:25:53
