<?php

if (!defined('BASEPATH'))
	  exit('No direct script access allowed');
class HouseOfficer extends CI_Controller
{

	// constructor
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('chamber_model');
		$this->load->model('patient_model');
		$this->load->model('appointment_model');
		$this->load->model('prescription_model');
		$this->load->model('validation_model');
		$this->load->model('billing_model');
		$this->load->model('user_model');
		$this->load->model('settings_model');
		$this->load->model('frontend_model');
		$this->form_validation->set_error_delimiters('<span style="color: red">', '</span>');
	}

	// default function
	public function index()
	{
		//if ($this->session->userdata('user_type') == 0)
			//redirect(site_url('login'), 'refresh');

		$user_type = $this->session->userdata('user_type');
		if($user_type == 0){
			redirect(site_url('admin/appointment'), 'refresh');
		}elseif($user_type == 1){
			redirect(site_url('doctor/appointment'), 'refresh');
		}elseif($user_type == 2){
			redirect(site_url('staff/appointment'), 'refresh');
		}elseif($user_type == 3){
			redirect(site_url('pharmacy/prescription'), 'refresh');
		}elseif($user_type == 4){
			redirect(site_url('medicalrecords/appointment'), 'refresh');
		}elseif($user_type == 5){
			redirect(site_url('houseofficer/appointment'), 'refresh');
		}

	}

	function appointment($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null){
			redirect(site_url('login'), 'refresh');
		}



		if ($param1 == 'create') {
			if ($this->appointment_model->create_appointment() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
				redirect(site_url('houseofficer/appointment_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('appointment_was_created_successfully'));
				redirect(site_url('houseofficer/appointment'), 'refresh');
			}

		}
		if ($param1 == 'manage') {
			if ($this->appointment_model->manage_appointment($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
				redirect(site_url('houseofficer/appointment_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('appointment_was_updated_successfully'));
				redirect(site_url('houseofficer/appointment_manage/' . $param2), 'refresh');
			}
		}


		if ($param1 == 'delete') {
			$this->db->where('appointment_id', $param2);
			$this->db->delete('appointment');
			$this->session->set_flashdata('success_message', get_phrase('appointment_was_deleted_successfully'));
			redirect(site_url('houseofficer/appointment'), 'refresh');
		}

		$page_data['page_name']  = 'appointment';
		$page_data['chamber']    =  $this->session->userdata['current_chamber'];
		$page_data['timestamp']  = strtotime(date('d-m-Y'));
		$page_data['page_title'] = get_phrase('appointment_list');
		$this->load->view('index', $page_data);
	}

	function appointments($timestamp, $clinic){
	$columns = array(
												0 =>'id',
												1 =>'mpi',
												2 =>'time',
												3 =>'name',
												4=> 'visited',
												5=> 'last_activity_by',
											);


			$limit = $this->input->post('length');
			$start = $this->input->post('start');
			$totalData = $this->appointment_model->allappointments_count($timestamp, $clinic);
			$totalFiltered = $totalData;


			if(empty($_POST['search']['value'])){
					$appointments = $this->appointment_model->get_appointments($limit, $start, $timestamp,$clinic);
			}else {
				$search = $_POST['search']['value'];
				$appointments =  $this->appointment_model->search_appointments($limit,$start,$timestamp,$clinic,$search);
				$totalFiltered = $this->appointment_model->search_appointments_count($limit,$start,$timestamp,$clinic,$search);
			}


			$data = array();
			if(!empty($appointments))
			{
					foreach ($appointments as $appointment)
					{
							$nestedData['id'] = $appointment->appointment_id;
							$nestedData['mpi'] = $appointment->patient_mpi;
							$nestedData['time'] = $appointment->schedule;
							$nestedData['name'] = get_patient_info_by_mpi($appointment->patient_mpi, 'first_name')." ".get_patient_info_by_mpi($appointment->patient_mpi, 'last_name');;
							$nestedData['visited'] = "$appointment->is_visited";
							$nestedData['last_activity_by'] = $appointment->accountability;
							$data[] = $nestedData;

					}
			}


			$totalData = $this->appointment_model->allappointments_count($timestamp, $clinic);
			$totalFiltered = $totalData;

			$json_data = array(
									"draw"            => intval($this->input->post('draw')),
									"recordsTotal"    => intval($totalData),
									"recordsFiltered" => intval($totalFiltered),
									"data"            => $data
									);
			echo json_encode($json_data);
}



	function apply_appointment_filter($date)
	{
		$page_data['timestamp']  = strtotime($date);
		$page_data['page_title'] = get_phrase('appointment_list');
		$this->load->view('houseofficer/appointment_list', $page_data);
	}

	function change_appointment_status($appointment_id, $status, $date)
	{
		$this->db->where('appointment_id', $appointment_id);
		$this->db->update('appointment', array(
			'is_visited' => $status,
			'appointment_completed_time' => strtotime($date),
			'visited_by' => $this->session->userdata('login_user_id'),
		));
		$page_data['timestamp']  = strtotime($date);
		$page_data['page_title'] = get_phrase('appointment_list');
		$this->load->view('houseofficer/appointment_list', $page_data);
	}

	function appointment_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']    = 'appointment_new';
		$page_data['patient_type'] = 'old';
		$page_data['page_title']   = get_phrase('new_appointment');
		$page_data['patients']     = $this->patient_model->get_patients_by_account();
		$page_data['houseofficers'] = $this->appointment_model->get_doctors_by_account();
		$page_data['chambers'] = $this->appointment_model->get_chambers_by_account();
		$this->load->view('index', $page_data);
	}

	function appointment_manage($appointment_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']    = 'appointment_manage';
		$page_data['chamber']    =  $this->session->userdata['current_chamber'];
		$page_data['patient_type'] = 'old';
		$page_data['appointment_id'] = $appointment_id;
		$page_data['page_title']   = get_phrase('manage_appointment');
		$page_data['patients']  = $this->patient_model->get_patients_by_account();
		$page_data['houseofficers'] = $this->appointment_model->get_doctors_by_account();

		$this->load->view('index', $page_data);
	}

	function print_appointments($date)
	{
		$page_data['timestamp']  = $date;
		$page_data['chamber']    =  $this->session->userdata['current_chamber'];
		$page_data['page_title'] = get_phrase('appointment_list');
		$page_data['page_name']       = 'print_appointments';
		$page_data['patients']  = $this->patient_model->get_patients_by_account();
		$page_data['houseofficers'] = $this->appointment_model->get_doctors_by_account();
		$this->load->view('index', $page_data);
	}



	function follow_up()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']    = 'follow_up';
		$page_data['patient_type'] = 'old';
		$page_data['page_title']   = get_phrase('follow_up');
		$page_data['houseofficers'] = $this->appointment_model->get_doctors_by_account();
		$page_data['patients']     = $this->patient_model->get_patients_by_account();
		$this->load->view('index', $page_data);
	}

	function load_appointments($appointment_date = '')
	{
		if ($appointment_date != '') {
			$timestamp = strtotime($appointment_date);
		} else {
			$timestamp = strtotime(date("d-m-y"));
		}
		$query                     = $this->appointment_model->get_appointment_by_day($timestamp);
		$page_data['appointments'] = $query;

		$data['result']            = $this->load->view('houseofficer/appointment_table', $page_data, TRUE);
		$data['appointment_count'] = $query->num_rows();
		echo json_encode($data);


	}

	function new_appointment($patient_type)
	{
		$page_data['chamber']      = $this->chamber_model->get_chamber_by_id($this->session->userdata('chamber_id'));
		$page_data['patients']     = $this->patient_model->get_patients_by_account();
		$page_data['patient_type'] = $patient_type;
		$page_data['page_name']    = 'appointment_new';
		$page_data['page_title']   = get_phrase('new_appointment');

		$this->load->view('index', $page_data);
	}

	function save_appointment()
	{

		$patient_type = $this->input->post('patient_type');

		if ($patient_type == 'old') {
			$this->form_validation->set_rules('patient_id', get_phrase('patient'), 'required|xss_clean');
		} else {
			$this->form_validation->set_rules('patient_phone', get_phrase('patient_phone'), 'required|xss_clean|min_length[11]|max_length[14]|callback_patient_phone_check');
			$this->form_validation->set_rules('patient_name', get_phrase('patient_name'), 'required|xss_clean');
		}
		$this->form_validation->set_rules('appointment_date', get_phrase('appointment_date'), 'required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			$this->new_appointment($patient_type);
		} else {
			$data = array();
			if ($patient_type == 'old') {
				$data['patient_id'] = $this->input->post('patient_id');
			} else {
				$data_patient          = array();
				$data_patient['name']  = $this->input->post('patient_name');
				$data_patient['phone'] = $this->input->post('patient_phone');
				$data['patient_id']    = $this->patient_model->insert_patient($data_patient);
			}

			$appointment_date   = $this->input->post('appointment_date');
			$data['timestamp']  = strtotime($appointment_date);
			$data['schedule']   = $this->input->post('schedule');
			$data['user_id']    = $this->session->userdata('login_user_id');
			$data['chamber_id'] = $this->session->userdata('chamber_id');

			$this->appointment_model->insert_appointment($data);
			$this->new_appointment($patient_type);
		}

	}

	function delete_appointment($appointment_id)
	{
		$this->appointment_model->delete_appointment($appointment_id);
		$data['msg'] = "Appointment Deleted";

		echo json_encode($data);

	}

	public function patient_phone_check($str)
	{
		$res = $this->patient_model->is_phone_exists($str);
		if ($res > 0) {
			$this->form_validation->set_message('patient_phone_check', get_phrase('mobile_in_use'));
			return FALSE;
		} else {
			return TRUE;
		}
	}

	function load_schedule_appointment_ajax($day = '', $clinic = '')
	{
		$page_data['chamber'] = $this->chamber_model->get_chamber_by_id($clinic);
		$page_data['day'] = $day;
		$this->load->view('admin/appointment_schedule_selector', $page_data);
	}

	function patient($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		if ($param1 == 'edit') {
			$result = $this->patient_model->edit_patient($param2);

			if ($result == 'success') {
				$this->session->set_flashdata('success_message', get_phrase('patient_profile_was_updated_successfully'));
				redirect(site_url('houseofficer/patient_profile/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
				redirect(site_url('houseofficer/patient_profile/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('patient_id', $param2);
			$this->db->delete('patient');
			$this->db->where('patient_id', $param2);
			$this->db->delete('prescription');
			$this->session->set_flashdata('success_message', get_phrase('patient_was_deleted_successfully'));
			redirect(site_url('houseofficer/patient'), 'refresh');
		}
		$page_data['page_name']  = 'patient';
		$page_data['page_title'] = get_phrase('patient_list');
		$this->load->view('index', $page_data);
	}


		function patients(){
		$columns = array(
                            0 =>'id',
                            1 =>'mpi',
                            2=> 'name',
                            3=> 'phone',
                            4=> 'age',
														5=> 'address',
                        );


				$limit = $this->input->post('length');
				$start = $this->input->post('start');
				$totalData = $this->patient_model->allpatients_count();
        $totalFiltered = $totalData;


				if(empty($_POST['search']['value'])){
						$patients = $this->patient_model->get_patients($limit, $start);
				}else {
					$search = $_POST['search']['value'];
				  $patients =  $this->patient_model->search_patients($limit,$start,$search);
					$totalFiltered = $this->patient_model->search_patients_count($limit,$start,$search);
				}


				$data = array();
        if(!empty($patients))
        {
            foreach ($patients as $patient)
            {
								$nestedData['id'] = $patient->patient_id;
                $nestedData['mpi'] = $patient->mpi;
                $nestedData['name'] = "$patient->first_name $patient->last_name";
                $nestedData['phone'] = $patient->phone;
                $nestedData['age'] = $patient->date_of_birth;
                $nestedData['address'] = $patient->address;
                $data[] = $nestedData;

            }
        }






				$totalData = $this->patient_model->allpatients_count();
        $totalFiltered = $totalData;

        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),
										"recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
	}


	public function search(){
				$limit = $this->input->post('length');
				$start = $this->input->post('start');
        $term = $this->input->get('term');
				$data =  $this->patient_model->search_patients($limit,$start,$term);
        echo json_encode($data);
  }

	public function searchPatients(){
      // Search term
      $searchTerm = $this->input->post('searchTerm');
      // Get users
      $response = $this->patient_model->searchPat($searchTerm);
      echo json_encode($response);
   }

	function patient_edit($patient_id)
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'patient_edit';
		$page_data['page_title'] = get_phrase('edit_patient');
		$page_data['patient_id'] = $patient_id;
		$this->load->view('index', $page_data);
	}

	function patient_profile($patient_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'patient_profile';
		$page_data['patient_id'] = $patient_id;
		$page_data['page_title'] = get_phrase('patient_profile');
		$this->load->view('index', $page_data);
	}

	function prescription($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		if ($param1 == 'create') {
			if ($this->prescription_model->create_prescription() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
				redirect(site_url('houseofficer/prescription_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('prescription_was_created_successfully'));
				redirect(site_url('houseofficer/prescription'), 'refresh');
			}
		}
		if ($param1 == 'manage') {
			if ($this->prescription_model->manage_prescription($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
				redirect(site_url('houseofficer/prescription_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('prescription_was_updated_successfully'));
				redirect(site_url('houseofficer/prescription_manage/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('prescription_id', $param2);
			$this->db->delete('prescription');
			$this->session->set_flashdata('success_message', get_phrase('prescription_was_deleted_successfully'));
			redirect(site_url('houseofficer/prescription'), 'refresh');
		}
		$page_data['page_name']  = 'prescription';
		$page_data['page_title'] = get_phrase('prescription_list');
		//$page_data['patients']    = $this->patient_model->get_patients_by_account();
		$this->load->view('index', $page_data);
	}



	function prescription_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'prescription_new';
		$page_data['page_title'] = get_phrase('new_prescription');
		$page_data['patients']   = $this->patient_model->get_patients_by_account();
		$this->load->view('index', $page_data);
	}

	function prescription_manage($prescription_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']       = 'prescription_manage';
		$page_data['prescription_id'] = $prescription_id;
		$page_data['page_title']      = get_phrase('manage_prescription');
		$this->load->view('index', $page_data);
	}

	function load_blank_prescription_entry($selector)
	{
		$page_data['selector'] = $selector;
		$this->load->view('houseofficer/prescription_entry_blank', $page_data);
	}

	function billing($param1 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		if ($param1 == 'create') {
			$result = $this->billing_model->create_invoice();
			if ($result == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
				redirect(site_url('houseofficer/invoice_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('invoice_was_created_successfully'));
				redirect(site_url('houseofficer/invoice_view/' . $result), 'refresh');
			}
		}

		$page_data['page_name']  = 'billing';
		$page_data['timestamp']  = strtotime(date('01-m-Y'));
		$page_data['timestamp2'] = strtotime(date('d-m-Y'));
		$page_data['selectedChamber'] = 'all';
		$page_data['page_title'] = get_phrase('invoices');
		$page_data['chambers'] = $this->db->get('chamber')->result_array();
		$this->load->view('index', $page_data);
	}

	/*
	function apply_invoice_filter($date)
	{
		$page_data['timestamp']  = strtotime($date);
		$page_data['page_title'] = get_phrase('invoices');
		$this->load->view('houseofficer/invoice_list', $page_data);
	}
	*/
	function apply_invoice_filter($date1, $date2, $chamber)
	{
		$page_data['timestamp']  = strtotime($date1);
		$page_data['timestamp2']  = strtotime($date2);
		$page_data['selectedChamber']  = $chamber;
		$page_data['chambers'] = $this->db->get('chamber')->result_array();
		$page_data['page_title'] = get_phrase('invoices');
		$this->load->view('houseofficer/invoice_list', $page_data);
	}

	function invoice_view($invoice_id)
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'invoice_view';
		$page_data['invoice_id'] = $invoice_id;
		$page_data['page_title'] = get_phrase('invoice');
		$this->load->view('index', $page_data);
	}

	function invoice_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'invoice_new';
		$page_data['page_title'] = get_phrase('create_invoice');
		$page_data['patients']   = $this->patient_model->get_patients_by_account();
		$this->load->view('index', $page_data);
	}

	function chamber($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		if ($param1 == 'create') {
			$this->chamber_model->create_chamber();
			$this->session->set_flashdata('success_message', get_phrase('chamber_was_created_successfully'));
			redirect(site_url('houseofficer/chamber'), 'refresh');
		}
		if ($param1 == 'edit') {
			$this->chamber_model->edit_chamber($param2);
			$this->session->set_flashdata('success_message', get_phrase('chamber_was_updated_successfully'));
			redirect(site_url('houseofficer/schedule/' . $param2), 'refresh');
		}
		if ($param1 == 'delete') {
			$this->db->where('chamber_id', $param2);
			$this->db->delete('chamber');
			$this->session->set_flashdata('success_message', get_phrase('chamber_was_deleted_successfully'));
			redirect(site_url('houseofficer/chamber'), 'refresh');
		}
		$page_data['page_name']  = 'chamber';
		$page_data['page_title'] = get_phrase('chamber_list');
		$this->load->view('index', $page_data);
	}

	function schedule($chamber_id)
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		$page_data['chamber']    = $this->chamber_model->get_chamber_by_id($chamber_id);
		$page_data['chamber_id'] = $chamber_id;
		$page_data['page_name']  = 'schedule';
		$page_data['page_title'] = get_phrase('schedule');
		$this->load->view('index', $page_data);
	}

	function staff($chamber_id)
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		$page_data['chamber_id'] = $chamber_id;
		$page_data['page_name']  = 'staff';
		$page_data['page_title'] = get_phrase('staff');
		$this->load->view('index', $page_data);
	}

	function createstaff($param1 = '', $param2 = '')
	{
		if ($param1 == 'create') {
			$chamber = $this->input->post('chamber');
			$data['status'] = $this->input->post('status');
			$data['user_type'] = $this->input->post('role');
			$data['name']      = $this->input->post('name');
			$data['email'] 	   = $this->input->post('email');
			$data['password']  = sha1($this->input->post('password'));
			$data['phone']      = $this->input->post('phone');
			$data['last_login'] = strtotime(date('d-m-Y'));
			$data['chamber'] = $chamber;

			if ($data['user_type'] === '2') {
				if($this->user_model->create_staff($data) == FALSE){
					$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
						redirect(site_url('houseofficer/staff/'.$chamber), 'refresh');
				}
				else{
					$this->session->set_flashdata('success_message', get_phrase('staff_was_created_successfully'));
					redirect(site_url('houseofficer/staff/'.$chamber), 'refresh');
				}
			}elseif ($data['user_type'] === '1') {
				if($this->user_model->create_houseofficer($data) == FALSE){
					$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
						redirect(site_url('houseofficer/staff/'.$chamber), 'refresh');
				}
				else{
					$this->session->set_flashdata('success_message', get_phrase('staff_was_created_successfully'));
					redirect(site_url('houseofficer/staff/'.$chamber), 'refresh');
				}
			}elseif ($data['user_type'] === '3') {
				if($this->user_model->create_pharmacist($data) == FALSE){
					$this->session->set_flashdata('error_message', get_phrase('the_phone_number_you_have_entered_is_not_valid_or_already_associated_with_another_account'));
						redirect(site_url('houseofficer/staff/'.$chamber), 'refresh');
				}
				else{
					$this->session->set_flashdata('success_message', get_phrase('staff_was_created_successfully'));
					redirect(site_url('houseofficer/staff/'.$chamber), 'refresh');
				}
			}


		}
		if ($param1 == 'edit') {
			$chamber = $this->uri->segment(4);
			$staff_id = $this->uri->segment(5);

			$data['status'] = $this->input->post('status');
			$data['user_type'] = $this->input->post('role');
			$data['name']      = $this->input->post('name');
			$data['email'] 	   = $this->input->post('email');
			$data['password']  = sha1($this->input->post('password'));
			$data['phone']      = $this->input->post('phone');
			$data['last_login'] = strtotime(date('d-m-Y'));
			$data['chamber'] = $chamber;

			if ($data['user_type'] === '1') {
				$result = $this->user_model->edit_houseofficer($chamber,$staff_id);
			}elseif ($data['user_type'] === '2') {
				$result = $this->user_model->edit_staff($chamber,$staff_id);
			}elseif ($data['user_type'] === '3') {
				$result = $this->user_model->edit_pharmacist($chamber,$staff_id);
			}
			if ($result == 'success') {
				$this->session->set_flashdata('success_message', get_phrase('staff_profile_was_updated_successfully'));
				redirect(site_url('houseofficer/staff/' . $chamber), 'refresh');
			} else {
				$this->session->set_flashdata('error_message', get_phrase('there_was_an_error_please_try_again'));
				redirect(site_url('houseofficer/staff/' . $chamber), 'refresh');
			}
		}
	}

	function editstaff($chamber,$user_id)
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		$page_data['staffinfo'] = $this->chamber_model->get_staff_by_id($user_id);
		$page_data['page_name']  = 'editstaff';
		$page_data['page_title'] = get_phrase('editstaff');
		//print_r($page_data);exit();
		$this->load->view('index', $page_data);
	}

	function profile($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		if ($param1 == 'change_password') {
			if ($this->user_model->update_password() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('your_password_could_not_be_changed'));
				redirect(site_url('houseofficer/profile'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('your_password_is_updated_successfully'));
				redirect(site_url('houseofficer/profile'), 'refresh');
			}
		}
		if ($param1 == 'change_info') {
			if ($this->user_model->change_houseofficer_info() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('your_profile_information_could_not_be_changed_due_to_invalid_phone_number_or_email'));
				redirect(site_url('houseofficer/profile'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('your_profile_info_is_updated_successfully'));
				redirect(site_url('houseofficer/profile'), 'refresh');
			}
		}
		$page_data['page_name']  = 'profile';
		$page_data['page_title'] = get_phrase('profile');
		$this->load->view('index', $page_data);
	}

	function deletestaff($param1, $param2){
			$this->db->where('user_id', $param1);
			$this->db->delete('staffusers');
			$this->session->set_flashdata('success_message', get_phrase('staff_was_deleted_successfully'));
			redirect(site_url('houseofficer/staff/'.$param2), 'refresh');
	}

	function load_practice_appointments($appointment_date = '')
	{

		if ($appointment_date != '') {
			$timestamp = strtotime($appointment_date);
		} else {
			$timestamp = strtotime(date("d-m-y"));
		}
		$query                     = $this->appointment_model->get_appointment_by_day($timestamp);
		$page_data['appointments'] = $query;

		$data['result']            = $this->load->view('houseofficer/appointment_table_practice', $page_data, TRUE);
		$data['appointment_count'] = $query->num_rows();
		echo json_encode($data);
	}

	function load_practice($appointment_id = '', $patient_id = '')
	{

		//$page_data['prescription'] = $this->appointment_model->create_or_get_prescription($appointment_id, $patient_id);
		$page_data['patient']      = $this->patient_model->get_patient_by_id($patient_id);

		$data['result'] = $this->load->view('houseofficer/practice_prescription', $page_data, TRUE);
		echo json_encode($data);
	}

	function save_schedule($chamber_id = '')
	{

		$schedules = array();
		$days      = $this->input->post('days');
		$open_days = $this->input->post('open_days');

		$morning_open    = $this->input->post('morning_open');
		$morning_close   = $this->input->post('morning_close');
		$afternoon_open  = $this->input->post('afternoon_open');
		$afternoon_close = $this->input->post('afternoon_close');
		$evening_open    = $this->input->post('evening_open');
		$evening_close   = $this->input->post('evening_close');
		$session4_open    = $this->input->post('session4_open');
		$session4_close   = $this->input->post('session4_close');
		$session5_open    = $this->input->post('session5_open');
		$session5_close   = $this->input->post('session5_close');
		$session6_open    = $this->input->post('session6_open');
		$session6_close   = $this->input->post('session6_close');
		$session7_open    = $this->input->post('session7_open');
		$session7_close   = $this->input->post('session7_close');
		$session8_open    = $this->input->post('session8_open');
		$session8_close   = $this->input->post('session8_close');
		$session9_open    = $this->input->post('session9_open');
		$session9_close   = $this->input->post('session9_close');
		$session10_open    = $this->input->post('session10_open');
		$session10_close   = $this->input->post('session10_close');
		$session11_open    = $this->input->post('session11_open');
		$session11_close   = $this->input->post('session11_close');
		$session12_open    = $this->input->post('session12_open');
		$session12_close   = $this->input->post('session12_close');
		$session13_open    = $this->input->post('session13_open');
		$session13_close   = $this->input->post('session13_close');
		$session14_open    = $this->input->post('session14_open');
		$session14_close   = $this->input->post('session14_close');
		$session15_open    = $this->input->post('session15_open');
		$session15_close   = $this->input->post('session15_close');
		$session16_open    = $this->input->post('session16_open');
		$session16_close   = $this->input->post('session16_close');
		$session17_open    = $this->input->post('session17_open');
		$session17_close   = $this->input->post('session17_close');
		$session18_open    = $this->input->post('session18_open');
		$session18_close   = $this->input->post('session18_close');
		$session19_open    = $this->input->post('session19_open');
		$session19_close   = $this->input->post('session19_close');
		$session20_open    = $this->input->post('session20_open');
		$session20_close   = $this->input->post('session20_close');
		$session21_open    = $this->input->post('session21_open');
		$session21_close   = $this->input->post('session21_close');
		$session22_open    = $this->input->post('session22_open');
		$session22_close   = $this->input->post('session22_close');
		$session23_open    = $this->input->post('session23_open');
		$session23_close   = $this->input->post('session23_close');
		$session24_open    = $this->input->post('session24_open');
		$session24_close   = $this->input->post('session24_close');
		$session25_open    = $this->input->post('session25_open');
		$session25_close   = $this->input->post('session25_close');
		$session26_open    = $this->input->post('session26_open');
		$session26_close   = $this->input->post('session26_close');
		$session27_open    = $this->input->post('session27_open');
		$session27_close   = $this->input->post('session27_close');
		$session28_open    = $this->input->post('session28_open');
		$session28_close   = $this->input->post('session28_close');



		foreach ($days as $key => $day) {
			$schedule = array();

			$schedule['day'] = $day;
			$schedule['key'] = $key;

			$is_open = in_array($key, $open_days);
			if ($is_open == 1) {

				//                print_r($key);
				$schedule['status']        = 'open';

				$schedule['morning_open']  = $morning_open[$key];
				$schedule['morning_close'] = $morning_close[$key];

				if ($schedule['morning_open'] != '' && $schedule['morning_close'] != '')
					$schedule['morning'] = $schedule['morning_open'] . ' - ' . $schedule['morning_close'];
				else
					$schedule['morning'] = '';

				$schedule['afternoon_open']  = $afternoon_open[$key];
				$schedule['afternoon_close'] = $afternoon_close[$key];

				if ($schedule['afternoon_open'] != '' && $schedule['afternoon_close'] != '')
					$schedule['afternoon'] = $schedule['afternoon_open'] . ' - ' . $schedule['afternoon_close'];
				else
					$schedule['afternoon'] = '';

				$schedule['evening_open']  = $evening_open[$key];
				$schedule['evening_close'] = $evening_close[$key];
				if ($schedule['evening_open'] != '' && $schedule['evening_close'] != '')
					$schedule['evening'] = $schedule['evening_open'] . ' - ' . $schedule['evening_close'];
				else
					$schedule['evening'] = '';

				$schedule['session4_open']  = $session4_open[$key];
				$schedule['session4_close'] = $session4_close[$key];
				if ($schedule['session4_open'] != '' && $schedule['session4_close'] != '')
						$schedule['session4'] = $schedule['session4_open'] . ' - ' . $schedule['session4_close'];
				else
				$schedule['session4'] = '';

				$schedule['session5_open']  = $session5_open[$key];
				$schedule['session5_close'] = $session5_close[$key];
				if ($schedule['session5_open'] != '' && $schedule['session5_close'] != '')
						$schedule['session5'] = $schedule['session5_open'] . ' - ' . $schedule['session5_close'];
				else
				$schedule['session5'] = '';

				$schedule['session6_open']  = $session6_open[$key];
				$schedule['session6_close'] = $session6_close[$key];
				if ($schedule['session6_open'] != '' && $schedule['session6_close'] != '')
						$schedule['session6'] = $schedule['session6_open'] . ' - ' . $schedule['session6_close'];
				else
				$schedule['session6'] = '';

				$schedule['session7_open']  = $session7_open[$key];
				$schedule['session7_close'] = $session7_close[$key];
				if ($schedule['session7_open'] != '' && $schedule['session7_close'] != '')
						$schedule['session7'] = $schedule['session7_open'] . ' - ' . $schedule['session7_close'];
				else
				$schedule['session7'] = '';

				$schedule['session8_open']  = $session8_open[$key];
				$schedule['session8_close'] = $session8_close[$key];
				if ($schedule['session8_open'] != '' && $schedule['session8_close'] != '')
						$schedule['session8'] = $schedule['session8_open'] . ' - ' . $schedule['session8_close'];
				else
				$schedule['session8'] = '';

				$schedule['session9_open']  = $session9_open[$key];
				$schedule['session9_close'] = $session9_close[$key];
				if ($schedule['session9_open'] != '' && $schedule['session9_close'] != '')
						$schedule['session9'] = $schedule['session9_open'] . ' - ' . $schedule['session9_close'];
				else
				$schedule['session9'] = '';

				$schedule['session9_open']  = $session9_open[$key];
				$schedule['session9_close'] = $session9_close[$key];
				if ($schedule['session9_open'] != '' && $schedule['session9_close'] != '')
						$schedule['session9'] = $schedule['session9_open'] . ' - ' . $schedule['session9_close'];
				else
				$schedule['session9'] = '';

				$schedule['session10_open']  = $session10_open[$key];
				$schedule['session10_close'] = $session10_close[$key];
				if ($schedule['session10_open'] != '' && $schedule['session10_close'] != '')
						$schedule['session10'] = $schedule['session10_open'] . ' - ' . $schedule['session10_close'];
				else
				$schedule['session10'] = '';

				$schedule['session11_open']  = $session11_open[$key];
				$schedule['session11_close'] = $session11_close[$key];
				if ($schedule['session11_open'] != '' && $schedule['session11_close'] != '')
						$schedule['session11'] = $schedule['session11_open'] . ' - ' . $schedule['session11_close'];
				else
				$schedule['session11'] = '';

				$schedule['session12_open']  = $session12_open[$key];
				$schedule['session12_close'] = $session12_close[$key];
				if ($schedule['session12_open'] != '' && $schedule['session12_close'] != '')
						$schedule['session12'] = $schedule['session12_open'] . ' - ' . $schedule['session12_close'];
				else
				$schedule['session12'] = '';

				$schedule['session13_open']  = $session13_open[$key];
				$schedule['session13_close'] = $session13_close[$key];
				if ($schedule['session13_open'] != '' && $schedule['session13_close'] != '')
						$schedule['session13'] = $schedule['session13_open'] . ' - ' . $schedule['session13_close'];
				else
				$schedule['session13'] = '';

				$schedule['session14_open']  = $session14_open[$key];
				$schedule['session14_close'] = $session14_close[$key];
				if ($schedule['session14_open'] != '' && $schedule['session14_close'] != '')
						$schedule['session14'] = $schedule['session14_open'] . ' - ' . $schedule['session14_close'];
				else
				$schedule['session14'] = '';

				$schedule['session15_open']  = $session15_open[$key];
				$schedule['session15_close'] = $session15_close[$key];
				if ($schedule['session15_open'] != '' && $schedule['session15_close'] != '')
						$schedule['session15'] = $schedule['session15_open'] . ' - ' . $schedule['session15_close'];
				else
				$schedule['session15'] = '';

				$schedule['session16_open']  = $session16_open[$key];
				$schedule['session16_close'] = $session16_close[$key];
				if ($schedule['session16_open'] != '' && $schedule['session16_close'] != '')
						$schedule['session16'] = $schedule['session16_open'] . ' - ' . $schedule['session16_close'];
				else
				$schedule['session16'] = '';

				$schedule['session17_open']  = $session17_open[$key];
				$schedule['session17_close'] = $session17_close[$key];
				if ($schedule['session17_open'] != '' && $schedule['session17_close'] != '')
						$schedule['session17'] = $schedule['session17_open'] . ' - ' . $schedule['session17_close'];
				else
				$schedule['session17'] = '';

				$schedule['session18_open']  = $session18_open[$key];
				$schedule['session18_close'] = $session18_close[$key];
				if ($schedule['session18_open'] != '' && $schedule['session18_close'] != '')
						$schedule['session18'] = $schedule['session18_open'] . ' - ' . $schedule['session18_close'];
				else
				$schedule['session18'] = '';

				$schedule['session19_open']  = $session19_open[$key];
				$schedule['session19_close'] = $session19_close[$key];
				if ($schedule['session19_open'] != '' && $schedule['session19_close'] != '')
						$schedule['session19'] = $schedule['session19_open'] . ' - ' . $schedule['session19_close'];
				else
				$schedule['session19'] = '';

				$schedule['session20_open']  = $session20_open[$key];
				$schedule['session20_close'] = $session20_close[$key];
				if ($schedule['session20_open'] != '' && $schedule['session20_close'] != '')
						$schedule['session20'] = $schedule['session20_open'] . ' - ' . $schedule['session20_close'];
				else
				$schedule['session20'] = '';

				$schedule['session21_open']  = $session21_open[$key];
				$schedule['session21_close'] = $session21_close[$key];
				if ($schedule['session21_open'] != '' && $schedule['session21_close'] != '')
						$schedule['session21'] = $schedule['session21_open'] . ' - ' . $schedule['session21_close'];
				else
				$schedule['session21'] = '';

				$schedule['session22_open']  = $session22_open[$key];
				$schedule['session22_close'] = $session22_close[$key];
				if ($schedule['session22_open'] != '' && $schedule['session22_close'] != '')
						$schedule['session22'] = $schedule['session22_open'] . ' - ' . $schedule['session22_close'];
				else
				$schedule['session22'] = '';

				$schedule['session23_open']  = $session23_open[$key];
				$schedule['session23_close'] = $session23_close[$key];
				if ($schedule['session23_open'] != '' && $schedule['session23_close'] != '')
						$schedule['session23'] = $schedule['session23_open'] . ' - ' . $schedule['session23_close'];
				else
				$schedule['session23'] = '';

				$schedule['session24_open']  = $session24_open[$key];
				$schedule['session24_close'] = $session24_close[$key];
				if ($schedule['session24_open'] != '' && $schedule['session24_close'] != '')
						$schedule['session24'] = $schedule['session24_open'] . ' - ' . $schedule['session24_close'];
				else
				$schedule['session24'] = '';

				$schedule['session25_open']  = $session25_open[$key];
				$schedule['session25_close'] = $session25_close[$key];
				if ($schedule['session25_open'] != '' && $schedule['session25_close'] != '')
						$schedule['session25'] = $schedule['session25_open'] . ' - ' . $schedule['session25_close'];
				else
				$schedule['session25'] = '';

				$schedule['session26_open']  = $session26_open[$key];
				$schedule['session26_close'] = $session26_close[$key];
				if ($schedule['session26_open'] != '' && $schedule['session26_close'] != '')
						$schedule['session26'] = $schedule['session26_open'] . ' - ' . $schedule['session26_close'];
				else
				$schedule['session26'] = '';

				$schedule['session27_open']  = $session27_open[$key];
				$schedule['session27_close'] = $session27_close[$key];
				if ($schedule['session27_open'] != '' && $schedule['session27_close'] != '')
						$schedule['session27'] = $schedule['session27_open'] . ' - ' . $schedule['session27_close'];
				else
				$schedule['session27'] = '';

				$schedule['session28_open']  = $session28_open[$key];
				$schedule['session28_close'] = $session28_close[$key];
				if ($schedule['session28_open'] != '' && $schedule['session28_close'] != '')
						$schedule['session28'] = $schedule['session28_open'] . ' - ' . $schedule['session28_close'];
				else
				$schedule['session28'] = '';


			} else {
				$schedule['status']        = 'closed';
				$schedule['morning_open']  = '';
				$schedule['morning_close'] = '';
				$schedule['morning']       = '';

				$schedule['afternoon_open']  = '';
				$schedule['afternoon_close'] = '';
				$schedule['afternoon']       = '';

				$schedule['evening_open']  = '';
				$schedule['evening_close'] = '';
				$schedule['evening']       = '';

				$schedule['session4_open']  = '';
				$schedule['session4_close'] = '';
				$schedule['session4']       = '';

				$schedule['session5_open']  = '';
				$schedule['session5_close'] = '';
				$schedule['session5']       = '';

				$schedule['session6_open']  = '';
				$schedule['session6_close'] = '';
				$schedule['session6']       = '';

				$schedule['session7_open']  = '';
				$schedule['session7_close'] = '';
				$schedule['session7']       = '';

				$schedule['session8_open']  = '';
				$schedule['session8_close'] = '';
				$schedule['session8']       = '';

				$schedule['session9_open']  = '';
				$schedule['session9_close'] = '';
				$schedule['session9']       = '';

				$schedule['session10_open']  = '';
				$schedule['session10_close'] = '';
				$schedule['session10']       = '';

				$schedule['session11_open']  = '';
				$schedule['session11_close'] = '';
				$schedule['session11']       = '';

				$schedule['session12_open']  = '';
				$schedule['session12_close'] = '';
				$schedule['session12']       = '';

				$schedule['session13_open']  = '';
				$schedule['session13_close'] = '';
				$schedule['session13']       = '';

				$schedule['session14_open']  = '';
				$schedule['session14_close'] = '';
				$schedule['session14']       = '';

				$schedule['session15_open']  = '';
				$schedule['session15_close'] = '';
				$schedule['session15']       = '';

				$schedule['session16_open']  = '';
				$schedule['session16_close'] = '';
				$schedule['session16']       = '';

				$schedule['session17_open']  = '';
				$schedule['session17_close'] = '';
				$schedule['session17']       = '';

				$schedule['session18_open']  = '';
				$schedule['session18_close'] = '';
				$schedule['session18']       = '';

				$schedule['session19_open']  = '';
				$schedule['session19_close'] = '';
				$schedule['session19']       = '';

				$schedule['session20_open']  = '';
				$schedule['session20_close'] = '';
				$schedule['session20']       = '';

				$schedule['session21_open']  = '';
				$schedule['session21_close'] = '';
				$schedule['session21']       = '';

				$schedule['session22_open']  = '';
				$schedule['session22_close'] = '';
				$schedule['session22']       = '';

				$schedule['session23_open']  = '';
				$schedule['session23_close'] = '';
				$schedule['session23']       = '';

				$schedule['session24_open']  = '';
				$schedule['session24_close'] = '';
				$schedule['session24']       = '';

				$schedule['session25_open']  = '';
				$schedule['session25_close'] = '';
				$schedule['session25']       = '';

				$schedule['session26_open']  = '';
				$schedule['session26_close'] = '';
				$schedule['session26']       = '';

				$schedule['session27_open']  = '';
				$schedule['session27_close'] = '';
				$schedule['session27']       = '';

				$schedule['session28_open']  = '';
				$schedule['session28_close'] = '';
				$schedule['session28']       = '';
			}
			array_push($schedules, $schedule);
		}

		//        print_r($schedules);
		//        die;
		$data             = array();
		$data['schedule'] = json_encode($schedules);
		$this->db->where('chamber_id', $chamber_id);
		$this->db->update('chamber', $data);
		$this->session->set_flashdata('success_message', get_phrase('schedule_was_updated_successfully'));
		redirect(site_url('houseofficer/schedule/' . $chamber_id), 'refresh');
	}


	function settings($param1 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		if ($param1 == 'update') {
			$this->settings_model->update_settings();
			$this->session->set_flashdata('success_message', get_phrase('settings_updated'));
			redirect(site_url('houseofficer/settings'), 'refresh');
		}

		$page_data['page_name']  = 'settings';
		$page_data['page_title'] = get_phrase('settings');
		$this->load->view('index', $page_data);
	}

	function change_chamber($chamber_id)
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
			$this->session->set_userdata('current_chamber', $chamber_id);
		/*$data['description'] = $chamber_id;
		$this->db->where('type', 'chamber_id');
		$this->db->update('settings', $data);*/

		//$this->session->set_flashdata('success_message', get_phrase('chamber_changed_successfully'));
		redirect(site_url('houseofficer/appointment'), 'refresh');
	}



	// print prescription
	function print_prescription($prescription_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');


		$page_data['prescription_id'] = $prescription_id;
		$page_data['page_name']       = 'print_prescription';
		$page_data['page_title']      = get_phrase('print_prescription');
		$this->load->view('index', $page_data);
	}




	function slidercontent($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		if ($param1 == 'create') {
			if ($this->frontend_model->create_slider() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/slider_content_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('slider_contentn_was_created_successfully'));
				redirect(site_url('houseofficer/slidercontent'), 'refresh');
			}
		}
		if ($param1 == 'manage') {
			if ($this->frontend_model->manage_slider($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/slider_content_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('slider_content_was_updated_successfully'));
				redirect(site_url('houseofficer/slider_content_manage/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('promo_id', $param2);
			$this->db->delete('fend_slidercontent');
			$this->session->set_flashdata('success_message', get_phrase('slider_was_deleted_successfully'));
			redirect(site_url('houseofficer/slidercontent'), 'refresh');
		}
		$page_data['page_name']  = 'slidercontentsettings';
		$page_data['page_title'] = get_phrase('slider_content_list');
		$this->load->view('index', $page_data);
	}

	function slider_content_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'slidercontent_new';
		$page_data['page_title'] = get_phrase('new_slider_content');
		$this->load->view('index', $page_data);
	}

	function slider_content_manage($promo_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']       = 'slider_content_manage';
		$page_data['promo_id'] = $promo_id;
		$page_data['page_title']      = get_phrase('manage_slider_content');
		$this->load->view('index', $page_data);
	}


	function promocontent($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		if ($param1 == 'create') {
			if ($this->frontend_model->create_promo() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/promo_content_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('promo_contentn_was_created_successfully'));
				redirect(site_url('houseofficer/promocontent'), 'refresh');
			}
		}
		if ($param1 == 'manage') {
			if ($this->frontend_model->manage_promo($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/promo_content_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('promo_content_was_updated_successfully'));
				redirect(site_url('houseofficer/promo_content_manage/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('promo_id', $param2);
			$this->db->delete('fend_promocontent');
			$this->session->set_flashdata('success_message', get_phrase('promo_was_deleted_successfully'));
			redirect(site_url('houseofficer/promocontent'), 'refresh');
		}
		$page_data['page_name']  = 'promo_content_settings';
		$page_data['page_title'] = get_phrase('promo_content_list');
		$this->load->view('index', $page_data);
	}

	function promo_content_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'promo_content_new';
		$page_data['page_title'] = get_phrase('new_promo_content');
		$this->load->view('index', $page_data);
	}

	function promo_content_manage($promo_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']       = 'promo_content_manage';
		$page_data['promo_id'] = $promo_id;
		$page_data['page_title']      = get_phrase('manage_promo_content');
		$this->load->view('index', $page_data);
	}


	function testimonial($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		if ($param1 == 'create') {
			if ($this->frontend_model->create_testimonial() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/testimonial_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('testimonial_created_successfully'));
				redirect(site_url('houseofficer/testimonial'), 'refresh');
			}
		}
		if ($param1 == 'manage') {
			if ($this->frontend_model->manage_testimonial($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/testimonial_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('testimonial_updated_successfully'));
				redirect(site_url('houseofficer/testimonial_manage/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('testimonial_id', $param2);
			$this->db->delete('fend_promocontent');
			$this->session->set_flashdata('success_message', get_phrase('promo_was_deleted_successfully'));
			redirect(site_url('houseofficer/testimonial'), 'refresh');
		}
		$page_data['page_name']  = 'testimonial_settings';
		$page_data['page_title'] = get_phrase('testimonial_list');
		$this->load->view('index', $page_data);
	}

	function testimonial_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'testimonial_new';
		$page_data['page_title'] = get_phrase('new_promo_content');
		$this->load->view('index', $page_data);
	}

	function testimonial_manage($testimonial_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']       = 'testimonial_manage';
		$page_data['testimonial_id'] = $testimonial_id;
		$page_data['page_title']      = get_phrase('manage_testimonial');
		$this->load->view('index', $page_data);
	}


	function qualification($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		if ($param1 == 'create') {
			if ($this->frontend_model->create_qualification() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/qaualfication_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('qualification_created_successfully'));
				redirect(site_url('houseofficer/qualification'), 'refresh');
			}
		}
		if ($param1 == 'manage') {
			if ($this->frontend_model->manage_qualification($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/qualification_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('qualification_updated_successfully'));
				redirect(site_url('houseofficer/qualification_manage/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('qualification_id', $param2);
			$this->db->delete('fend_qualification');
			$this->session->set_flashdata('success_message', get_phrase('qualification_was_deleted_successfully'));
			redirect(site_url('houseofficer/qualification'), 'refresh');
		}
		$page_data['page_name']  = 'qualification_settings';
		$page_data['page_title'] = get_phrase('testimonial_list');
		$this->load->view('index', $page_data);
	}

	function qualification_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'qualification_new';
		$page_data['page_title'] = get_phrase('new_qualification');
		$this->load->view('index', $page_data);
	}

	function qualification_manage($qualification_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']       = 'qualification_manage';
		$page_data['qualification_id'] = $qualification_id;
		$page_data['page_title']      = get_phrase('manage_qualification');
		$this->load->view('index', $page_data);
	}

	function blog($param1 = '', $param2 = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		if ($param1 == 'create') {
			if ($this->frontend_model->create_blog() == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/blog_new'), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('qualification_created_successfully'));
				redirect(site_url('houseofficer/blog'), 'refresh');
			}
		}
		if ($param1 == 'manage') {
			if ($this->frontend_model->manage_blog($param2) == FALSE) {
				$this->session->set_flashdata('error_message', get_phrase('please_try_again'));
				redirect(site_url('houseofficer/blog_manage/' . $param2), 'refresh');
			} else {
				$this->session->set_flashdata('success_message', get_phrase('qualification_updated_successfully'));
				redirect(site_url('houseofficer/blog_manage/' . $param2), 'refresh');
			}
		}
		if ($param1 == 'delete') {
			$this->db->where('blog_id', $param2);
			$this->db->delete('fend_blog');
			$this->session->set_flashdata('success_message', get_phrase('qualification_was_deleted_successfully'));
			redirect(site_url('houseofficer/qualification'), 'refresh');
		}
		$page_data['page_name']  = 'blog_settings';
		$page_data['page_title'] = get_phrase('blog_list');
		$this->load->view('index', $page_data);
	}

	function blog_new()
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']  = 'blog_new';
		$page_data['page_title'] = get_phrase('new_blog');
		$this->load->view('index', $page_data);
	}

	function blog_manage($blog_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');
		$page_data['page_name']       = 'blog_manage';
		$page_data['blog_id'] 		  = $blog_id;
		$page_data['page_title']      = get_phrase('blog_manage');
		$this->load->view('index', $page_data);
	}

	function blog_detail($blog_id = '')
	{
		if ($this->session->userdata('user_type') != 5 || $this->session->userdata('user_type') == null)
			redirect(site_url('login'), 'refresh');

		$page_data['blog_id'] 		  = $blog_id;
		$this->load->view('frontend/blog-single', $page_data);
	}


}
