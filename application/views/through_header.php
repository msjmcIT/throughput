<?php date_default_timezone_set('UTC');
 ?>
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <!-- Toggle icon for mobile view -->
        <a class="navbar-toggle hidden-sm hidden-md hidden-lg "
           href="javascript:void(0)" data-toggle="collapse"
           data-target=".navbar-collapse"><i class="ti-menu"></i></a>
        <!-- Logo -->
        <div class="top-left-part" style="width: 50%;">
            <a class="logo" href="<?php echo site_url('doctor');?>">
                <!-- Logo icon image, you can use font-icon also -->
                <b><img width="40" src="<?php echo base_url('uploads/'.get_settings('logo'));?>"></b>
                <span class="hidden-xs" style="font-size: 14px;">
                    <strong>MSJMC</strong> - Throughput
                </span>
            </a>
        </div>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        <ul class="nav navbar-top-links navbar-left hidden-xs">
            <li><a href="javascript:void(0)"
                   class="open-close hidden-xs waves-effect waves-light">
                    <i class="icon-arrow-left-circle ti-menu"></i></a></li>
        </ul>
        <!-- This is the message dropdown -->



    </div>
</nav>
