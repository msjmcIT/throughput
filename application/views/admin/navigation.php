<style type="text/css">
    .floatingHeader {
          position: fixed;
          top: 0;
          visibility: hidden;
        }
</style>

<div class="navbar-default sidebar persist-area" role="navigation" >
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav persist-header" id="side-menu">
          <li>
              <a href="<?php echo site_url('admin/admittance');?>"
                 class="waves-effect <?php if ($page_name == 'admittance' || $page_name == 'admittance' || $page_name == 'throughput') echo 'active';?>">
                  <i class="icon-speedometer fa-fw"></i>
                  <span class="hide-menu"><?php echo get_phrase('admittance'); ?></span>
              </a>
          </li>
            <li>
                <a href="<?php echo site_url('admin/patient');?>"
                   class="waves-effect <?php if ($page_name == 'patient_profile' || $page_name == 'patient') echo 'active';?>">
                    <i class="ti-user fa-fw"></i>
                    <span class="hide-menu"><?php echo get_phrase('patients'); ?></span>
                </a>
            </li>


        </ul>
    </div>
</div>

<script type="text/javascript">
function UpdateTableHeaders() {
   $(".persist-area").each(function() {

       var el             = $(this),
           offset         = el.offset(),
           scrollTop      = $(window).scrollTop(),
           floatingHeader = $(".floatingHeader", this)

       if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
           floatingHeader.css({
            "visibility": "visible"
           });
       } else {
           floatingHeader.css({
            "visibility": "hidden"
           });
       };
   });
}

// DOM Ready
$(function() {

   var clonedHeaderRow;

   $(".persist-area").each(function() {
       clonedHeaderRow = $(".persist-header", this);
       clonedHeaderRow
         .before(clonedHeaderRow.clone())
         .css("width", clonedHeaderRow.width())
         .addClass("floatingHeader");

   });

   $(window)
    .scroll(UpdateTableHeaders)
    .trigger("scroll");

});
</script>
