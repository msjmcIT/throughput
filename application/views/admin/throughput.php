<?php



 ?>

<div class="row" style="margin-top: 20px;">
    <div class="col-md-12">
      <div class="table-responsive">
        <table id="myTable" class="table table-bordered">
          <thead>
            <tr>
              <th><?php echo get_phrase('iniitials');?></th>
              <th><?php echo get_phrase('mpi');?></th>
              <th><?php echo get_phrase('time_in');?></th>
              <th><?php echo get_phrase('time_triaged');?></th>
              <th><?php echo get_phrase('time_seen');?></th>
              <th><?php echo get_phrase('ESI');?></th>
              <th><?php echo get_phrase('disposition');?></th>
              <th><?php echo get_phrase('time_out');?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
</div>


<script type="text/javascript">





$(document).ready(function() {
  // hide inputs for new patient
  $('#new_patient_data_inputs').hide();
  $('#chamber_inputs').hide();

  $('#add_chamber_button').click(function() {
    $('#chamber_inputs').show(200);
  });

  $('#myTable_length').hide();

  $('#myTable').DataTable( {
      "pageLength": 7,
      "processing": true,
      "serverSide": true,
      "searching":false,

      "ajax":{
       "url": "<?php echo base_url('index.php/admin/admittances/');?>",
       "dataType": "json",
       "type": "POST",
       "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
        },
        "columns": [
            { "data": "name" },
            { "data": "mpi" },
            { "data": "time_in" },
            { "data": "time_triaged" },
            { "data": "time_seen" },
            { "data": "esi" },
            { "data": "disposition" },
            { "data": "time_out" },
         ]
  }

);/***end of my table init function*/


      
      if ('time < 3 hours') {
        'color is grey'
      }else if ('time > 3 && time < 6') {
        'color is yellow'
      }else if ('time > 6 && time < 12') {
        'color is blue '
      }else if ('time >  12') {
        'color is red '
      }



  $("#selUser").select2({
       ajax: {
         url: '<?= base_url() ?>index.php/admin/searchPatients',
         type: "post",
         dataType: 'json',
         data: function (params) {
            return {
            searchTerm: params.term // search term
            };
         },
         processResults: function (response) {
            return {
               results: response
            };
         },
         cache: true
       },
       minimumInputLength: 2,
   });
});

function hide_inputs() {
  $('#esi').val('');
  $('#disposition').val('');
  $('#chamber_inputs').hide(200);
}

function show_inputs() {
  $('#patient_type').val('new');
  $('#new_patient_data_inputs').fadeIn('slow');
  $('#old_patient_input').hide();
  $('#patientName').attr('required', true);
  $('#patientPhone').attr('required', true);
  $('#patientAddress').attr('required', true);
  $('#patientDoctor').attr('required', true);
}


</script>
