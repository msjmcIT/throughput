<?php

  $info = $this->db->get_where('admittance', array(
    'admittance_id' => $appointment_id
  ));

   $apt_info = $info->result_array();
   foreach ($apt_info as $row);

   $patient_id = $row['patient_mpi'];

   $patient_info = $this->db->get_where('patient', array(
     'mpi' => $patient_id))->result_array();
     foreach ($patient_info as $patient_rows);

?>
<div class="row">
  <div class="col-lg-12">
    <div class="white-box">
      <form action="<?php echo site_url('admin/admittance/manage/' . $row['admittance_id']);?>" method="post">
        <div>
          <div class="col-sm-12 col-xs-12">
            <div class="col-sm-2 checkbox checkbox-success checkbox-circle">
                  <input type="checkbox" name="visited_by_doctor" id="visited_by_doctor"
                    <?php if ($row['visited_by_doctor'] == 'on') echo 'checked disabled';?>>
                  <label><?php echo get_phrase('seen_by_doctor'); ?></label>
                </div>
          </div>

          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('MPI'); ?></label>
                <input type="text" class="form-control" id="mpi" placeholder="MPI"
                  name="mpi" value="<?php echo $row['patient_mpi']; ?>" disabled>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('first_name'); ?></label>
                <input type="text" class="form-control" id="patientFirstName" placeholder="<?php echo get_phrase('first_name');?>"
                  name="first_name" value="<?php echo $patient_rows['first_name']; ?>" disabled>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('last_name'); ?></label>
                <input type="text" class="form-control" id="patientLastName" placeholder="<?php echo get_phrase('last_name');?>"
                  name="last_name" value="<?php echo $patient_rows['last_name']; ?>" disabled>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('telephone_number'); ?></label>
                <input type="text" class="form-control" id="patientPhone" placeholder="<?php echo get_phrase('telephone_number');?>"
                  name="phone" value="<?php echo $patient_rows['phone']; ?>" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('telephone_number_2'); ?></label>
                <input type="text" class="form-control" id="patientPhone2" placeholder="<?php echo get_phrase('telephone_number_2');?>"
                  name="phone2" value="<?php echo $patient_rows['phone2']; ?>" disabled>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group ">
                <label><?php echo get_phrase('date_of_birth'); ?></label>
                <div class="input-group" style="z-index: 1000;">
                  <input id="date_of_birth" type="text" class="form-control"
                    value="<?php echo $patient_rows['date_of_birth']; ?>"
                    name="date_of_birth" disabled>
                  <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('gender'); ?></label>
                <select class="selectpicker" data-style="form-control" name="gender" id="gender" disabled>
                    <option value="F" <?php if ($patient_rows['gender'] == 'F') echo 'checked'; ?>>
                        <?php echo get_phrase('female'); ?>
                    </option>
                    <option value="M" <?php if ($patient_rows['gender'] == 'M') echo 'checked'; ?>>
                        <?php echo get_phrase('male'); ?>
                    </option>
                </select>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('Address'); ?></label>
                <input type="text" class="form-control" id="patientAddress" placeholder="<?php echo get_phrase('address');?>"
                  name="address" value="<?php echo $patient_rows['address']; ?>" disabled>
              </div>
            </div>

          </div>
        </div><!-------------END OF NEW PATEINT--------->
        <div class="row">
          <div class="col-sm-6">

            <div class="form-group">
              <label><?php echo get_phrase('esi_triage'); ?></label>
              <select class="selectpicker" data-style="form-control" name="esi_triage" id="esi_triage">
                <option value="">
                  <?php echo get_phrase('select_esi_triage'); ?>
                </option>
                  <option value="1"  <?php if ($row['esi_triage'] == '1') echo 'selected'; ?>>
                      <?php echo get_phrase('1'); ?>
                  </option>
                  <option value="2" <?php if ($row['esi_triage'] == '2') echo 'selected'; ?>>
                      <?php echo get_phrase('2'); ?>
                  </option>
                  <option value="3" <?php if ($row['esi_triage'] == '3') echo 'selected'; ?>>
                      <?php echo get_phrase('3'); ?>
                  </option>
                  <option value="4" <?php if ($row['esi_triage'] == '4') echo 'selected'; ?>>
                      <?php echo get_phrase('4'); ?>
                  </option>
                  <option value="5" <?php if ($row['esi_triage'] == '5') echo 'selected'; ?>>
                      <?php echo get_phrase('5'); ?>
                  </option>
              </select>
            </div>


          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label><?php echo get_phrase('disposition'); ?></label>
              <select class="selectpicker" data-style="form-control" name="disposition" id="disposition">
                <option value="">
                  <?php echo get_phrase('select_disposition'); ?>
                </option>
                  <option value="ER review" <?php if ($row['disposition'] == 'ER review') echo 'selected'; ?>>
                      <?php echo get_phrase('ER_review'); ?>
                  </option>
                  <option value="reffered" <?php if ($row['disposition'] == 'reffered') echo 'selected'; ?>>
                      <?php echo get_phrase('reffered'); ?>
                  </option>
                  <option value="admitted" <?php if ($row['disposition'] == 'admitted') echo 'selected'; ?>>
                      <?php echo get_phrase('admitted'); ?>
                  </option>
                  <option value="discharged" <?php if ($row['disposition'] == 'discharged') echo 'selected'; ?>>
                      <?php echo get_phrase('discharged'); ?>
                  </option>

              </select>
            </div>
          </div>
        </div><!--END OF ADMITTANCE-->


        <input id="patient_type" type="hidden" name="patient_type" value="old">
        <input id="appointment_type" type="hidden" name="appointment_type" value="NP">

        <button type="button"class="btn btn-success">
          <a style="color:#fff;" href="<?php echo site_url('admin/patient_profile/');?><?php echo $patient_rows['mpi'] ?>"><?php echo get_phrase('view_patient_profile'); ?></a>
        </button>
        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">
          <?php echo get_phrase('update'); ?>
        </button>


      </form>


    </div><!--end of white box -->
  </div><!--end of column 12 -->
</div><!--end of row -->
<script type="text/javascript">
$("form").submit(function() {
  $("#visited_by_doctor").prop("disabled", false);
});
</script>
