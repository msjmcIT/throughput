
<?php
 ini_set('memory_limit', '1024M');
?>
<div class="row">

 <div class="col-sm-12">
   <div class="white-box">
     <h3 class="box-title m-b-30"><?php echo get_phrase('patient_list'); ?></h3>
     <div class="table-responsive">
       <table id="patient" class="table table-striped">
         <thead>
           <tr>
             <th>#</th>
             <th><?php echo get_phrase('mpi'); ?></th>
             <th><?php echo get_phrase('name'); ?></th>
             <th><?php echo get_phrase('phone'); ?></th>
             <th><?php echo get_phrase('d_o_b'); ?></th>
             <th><?php echo get_phrase('address'); ?></th>
             <th><?php echo get_phrase('options'); ?></th>
           </tr>
         </thead>
         <tfoot>
          <tr>
            <th>#</th>
            <th><?php echo get_phrase('mpi'); ?></th>
            <th><?php echo get_phrase('name'); ?></th>
            <th><?php echo get_phrase('phone'); ?></th>
            <th><?php echo get_phrase('d_o_b'); ?></th>
            <th><?php echo get_phrase('address'); ?></th>
            <th><?php echo get_phrase('options'); ?></th>
          </tr>
      </tfoot>
       </table>
     </div>
   </div>
 </div>
</div>




<script type="text/javascript">
 $(document).ready(function() {

   $('#patient').DataTable( {
       "pageLength": 100,
       "processing": true,
       "serverSide": true,
       "searching":true,
       "ajax":{
        "url": "<?php echo base_url('index.php/admin/patients') ?>",
        "dataType": "json",
        "type": "POST",
        "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
         },
         "columns": [
             { "data": "id" },
             { "data": "mpi" },
             { "data": "name" },
             { "data": "phone" },
             { "data": "age" },
             { "data": "address" },
             {
                sortable: false,
                "render": function ( data, type, full, meta ) {
                    var linkID = full.mpi;
                    return '<a  href="<?php echo site_url('admin/patient_profile');?>/'+linkID+'" class="fcbtn btn btn-info btn-outline btn-1d btn-sm" role="button">Manage Patient</a>  <button type="button" onclick="delete_pateient('+linkID+')" class="fcbtn btn btn-danger btn-outline btn-1d btn-sm">Delete</button>';


                }
            },

          ]
   } );



   });

 function delete_pateient(patient_id) {
   swal({
     title: "Are you sure?",
     text: "The patient and it's content will be deleted permanently !",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: "#DD6B55",
     confirmButtonClass: 'btn-warning',
     confirmButtonText: "Yes, delete it!",
     closeOnConfirm: false
   }, function () {
     swal("Deleted!", "The patient is deleted", "success");
     setTimeout(function() {
       window.location = "<?php echo site_url('admin/patient/delete/');?>" + patient_id;
     }, 1000);
   });
 }
</script>
