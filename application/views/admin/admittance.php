<div class="row" id="chamber_inputs">
  <div class="col-lg-12">
    <div class="white-box">
      <form action="<?php echo site_url('admin/admittance/create');?>" method="post">
        <div class="row" id="old_patient_input">
          <div class="col-sm-6">
            <div class="form-group">
                <button type="button" class="fcbtn btn btn-info btn-outline btn-1d"
                            onclick="show_inputs()">
                        <i class="fa fa-plus"></i> &nbsp; <?php echo get_phrase('new_patient'); ?>
                </button>
              </div>

              <div class="form-group">
                <label for="patientSelect">Patient</label style="display: block;">
                <select class="form-control select2" id='selUser' name="patient_mpi" style="width:100%">
                    <option value='0'>Enter Patient MPI</option>
                </select>
              </div>
          </div>
        </div>



        <div id="new_patient_data_inputs">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('MPI'); ?></label>
                <input type="text" class="form-control" id="mpi" placeholder="MPI"
                  name="mpi" value="">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('first_name'); ?></label>
                <input type="text" class="form-control" id="patientFirstName" placeholder="<?php echo get_phrase('first_name');?>"
                  name="first_name" value="">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('last_name'); ?></label>
                <input type="text" class="form-control" id="patientFirstName" placeholder="<?php echo get_phrase('last_name');?>"
                  name="last_name" value="">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('telephone_number'); ?></label>
                <input type="text" class="form-control" id="patientPhone" placeholder="<?php echo get_phrase('telephone_number');?>"
                  name="phone" value="">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('telephone_number_2'); ?></label>
                <input type="text" class="form-control" id="patientPhone2" placeholder="<?php echo get_phrase('telephone_number_2');?>"
                  name="phone2" value="">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group ">
                <label><?php echo get_phrase('date_of_birth'); ?></label>
                <div class="input-group" style="z-index: 1000;">
                  <input id="date_of_birth" type="text" class="form-control"
                    value=""
                    name="date_of_birth">
                  <span class="input-group-addon"><i class="icon-calender"></i></span>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('gender'); ?></label>
                <select class="selectpicker" data-style="form-control" name="gender">
                    <option value="F">
                        <?php echo get_phrase('female'); ?>
                    </option>
                    <option value="M">
                        <?php echo get_phrase('male'); ?>
                    </option>
                </select>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label><?php echo get_phrase('Address'); ?></label>
                <input type="text" class="form-control" id="patientAddress" placeholder="<?php echo get_phrase('address');?>"
                  name="address" value="">
              </div>
            </div>

          </div>
        </div><!-------------END OF NEW PATEINT--------->
        <div class="row">
          <div class="col-sm-6">

            <div class="form-group">
              <label><?php echo get_phrase('esi_triage'); ?></label>
              <select class="selectpicker" data-style="form-control" name="esi_triage" id="esi_triage">
                  <option value="">
                    <?php echo get_phrase('select_esi_triage'); ?>
                  </option>
                  <option value="1">
                      <?php echo get_phrase('1'); ?>
                  </option>
                  <option value="2">
                      <?php echo get_phrase('2'); ?>
                  </option>
                  <option value="3">
                      <?php echo get_phrase('3'); ?>
                  </option>
                  <option value="4">
                      <?php echo get_phrase('4'); ?>
                  </option>
                  <option value="5">
                      <?php echo get_phrase('5'); ?>
                  </option>
              </select>
            </div>


          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label><?php echo get_phrase('disposition'); ?></label>
              <select class="selectpicker" data-style="form-control" name="disposition" id="disposition">
                <option value="">
                  <?php echo get_phrase('select_disposition'); ?>
                </option>
                  <option value="ER review">
                      <?php echo get_phrase('ER_review'); ?>
                  </option>
                  <option value="reffered">
                      <?php echo get_phrase('reffered'); ?>
                  </option>
                  <option value="admitted">
                      <?php echo get_phrase('admitted'); ?>
                  </option>
                  <option value="discharged">
                      <?php echo get_phrase('discharged'); ?>
                  </option>

              </select>
            </div>
          </div>
        </div><!--END OF ADMITTANCE-->


        <input id="patient_type" type="hidden" name="patient_type" value="old">
        <input id="appointment_type" type="hidden" name="appointment_type" value="NP">

        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">
          <?php echo get_phrase('admit'); ?>
        </button>

        <button type="button" class="btn btn-danger waves-effect waves-light m-r-10" class="btn btn-default btn-block" onclick="hide_inputs()">
          <i class="fa fa-times"></i>
        </button>


      </form>


    </div><!--end of white box -->
  </div><!--end of column 12 -->
</div><!--end of row -->


  <div class="row">
    <div class="col-md-2"  style="margin-top: 10px">
      <button style="width: 100%" type="button" id="add_chamber_button" class="btn btn-info btn-1d">
        <i class="fa fa-plus"></i> &nbsp; <?php echo get_phrase('admit_patient'); ?>
      </button>
    </div>
    </div>


  <div class="row" style="margin-top: 20px;">
    <div class="col-md-12">
      <div class="table-responsive">
        <table id="myTable" class="table table-bordered">
          <thead>
            <tr class="table_row">
              <th><?php echo get_phrase('iniitials');?></th>
              <th><?php echo get_phrase('mpi');?></th>
              <th><?php echo get_phrase('time_in');?></th>
              <th><?php echo get_phrase('time_triaged');?></th>
              <th><?php echo get_phrase('time_seen');?></th>
              <th><?php echo get_phrase('ESI');?></th>
              <th><?php echo get_phrase('disposition');?></th>
              <th><?php echo get_phrase('time_out');?></th>
              <th><?php echo get_phrase('action');?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>


<script type="text/javascript">





$(document).ready(function() {

  // hide inputs for new patient
  $('#new_patient_data_inputs').hide();
  $('#chamber_inputs').hide();

  $('#add_chamber_button').click(function() {
    $('#chamber_inputs').show(200);
  });

  $('#myTable_length').hide();

  var table = $('#myTable').DataTable( {
      createdRow: function ( row, data, index ) {
         $(row).addClass('table_row');
      },
      "pageLength": 7,
      "processing": true,
      "serverSide": true,
      "searching":true,
      "ajax":{
       "url": "<?php echo base_url('index.php/admin/admittances/');?>",
       "dataType": "json",
       "type": "POST",
       "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
        },
      "columns": [
            { "data": "name" },
            { "data": "mpi" },
            { "data": "time_in",
              className: "time_in", },
            { "data": "time_triaged" },
            { "data": "time_seen" },
            { "data": "esi" },
            { "data": "disposition" },
            { "data": "time_out",
              className: "time_out",},
            {
               sortable: false,
               "render": function ( data, type, full, meta ) {
                    var linkID = full.id;
                   return '<a style="width:100%;" href="<?php echo site_url('admin/admittance_manage');?>/'+linkID+'" class="fcbtn btn btn-info btn-outline btn-1d btn-sm" role="button">Edit</a>';


               }
           },
         ],
         "fnInitComplete": function(oSettings, json) {
           colorize();
    }

  } );/***end of my table init function*/

  //get all time_in


    function colorize() {
      var color;
      var times_in = document.getElementsByClassName('time_in');
      var times_out = document.getElementsByClassName('time_out');
      var rows = document.getElementsByClassName('table_row');

      for (var i = 0; i < times_in.length; i++) {
        //console.log(rows[i]);
        var dataTimesIn = new Date(times_in[i].innerHTML.substr(0, 19));
        var dataTimesOut = new Date(times_out[i].innerHTML.substr(0, 19));

        var timein = dataTimesIn;
        //console.log(timein);

        var timeout = dataTimesOut;
        //console.log(timeout);

        var currentdate = new Date();
        console.log('current',currentdate);
        console.log('timein',timein);

        var difference = Math.abs(timein - currentdate);
        var hourDifference = Math.round(difference  / 1000 / 3600);
        console.log(hourDifference);

        if (hourDifference <= 3) {
         rows[i].style.color = 'grey';
         console.log('less than 3');
        }else if (hourDifference <= 6) {
          rows[i].style.color = 'orange';
          console.log('less than 6');
        }else if (hourDifference <= 12) {
          rows[i].style.color = 'blue';
          console.log('less than 12');
        }else if (hourDifference > 12) {
          rows[i].style.color = 'red';
          console.log('more than 12');
        }
        //apply color to elments based on times
      }
    }


  $("#selUser").select2({
       ajax: {
         url: '<?= base_url() ?>index.php/admin/searchPatients',
         type: "post",
         dataType: 'json',
         data: function (params) {
            return {
            searchTerm: params.term // search term
            };
         },
         processResults: function (response) {
            return {
               results: response
            };
         },
         cache: true
       },
       minimumInputLength: 2,
   });




});

function hide_inputs() {
  $('#esi').val('');
  $('#disposition').val('');
  $('#chamber_inputs').hide(200);
}

function show_inputs() {
  $('#patient_type').val('new');
  $('#new_patient_data_inputs').fadeIn('slow');
  $('#old_patient_input').hide();
  $('#patientName').attr('required', true);
  $('#patientPhone').attr('required', true);
  $('#patientAddress').attr('required', true);
  $('#patientDoctor').attr('required', true);
}






</script>
