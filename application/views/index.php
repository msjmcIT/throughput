<?php
ini_set('display_errors', 1);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include 'metas.php';?>

    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url('uploads/favicon.png');?>">

    <title>MSJMC | Outpatient Clinic</title>

    <?php include 'includes_top.php';?>
</head>

<body>

<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>

<div id="wrapper">
    <?php
    $user_type = $this->session->userdata('user_type');
      include 'admin/header.php';
    ?>
    <?php
        $user_type = $this->session->userdata('user_type');
          include 'admin/navigation.php';
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">
            <?php
                $user_type = $this->session->userdata('user_type');
                    if($user_type == 0){
                        include 'admin/'.$page_name.'.php';
                    }
                ?>
        </div>
        <?php include 'footer.php';?>
    </div>
</div>
    <?php include 'includes_bottom.php';?>
</body>

</html>
