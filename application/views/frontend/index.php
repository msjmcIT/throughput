<!DOCTYPE html>
<html lang="en">


<?php
//print_r($address);


$userdata = $this->db->get('users')->row();
$email = get_user_info_by_id($userdata->user_id, 'email');
$phone = get_user_info_by_id($userdata->user_id, 'phone');
$SliderContentSettings = $this->db->get('fend_slidercontent')->result_array();
$PromoContentSettings = $this->db->get('fend_promocontent')->result_array();
$ChambersList = $this->db->get('chamber')->result_array();
$TestimonialSettings = $this->db->get('fend_testimonial')->result_array();
$Qualification = $this->db->get('fend_qualification')->row();
$BlogSettings = $this->db->get('fend_blog')->result_array();
$address = $this->chamber_model->get_address();

if (is_array($address)) {
	# code...
	$text_address = "";
	foreach ($address as $key => $value) {
	# code...
	$text_address .=  $value['address'].' ';
	}
}

// print '<pre>';
// print_r($SliderContentSettings);
// print '</pre>';
// exit();
?>

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Doctor Appointment Booking System</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url()?>uploads/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet"> -->

  <!-- <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'> -->

  <!-- <link href="https://fonts.googleapis.com/css?family=sans-serif:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet"> -->

  <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

 <link href="<?php echo base_url('assets/backend/css/fontawesome-all.min.css') ?>" rel="stylesheet" type="text/css" />


  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url()?>assets/doctor/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/doctor/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/doctor/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/doctor/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/doctor/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/doctor/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url()?>assets/doctor/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url('assets/doctor/vendor/bootstrap/css/bootstrap-datepicker.css');?>"
      rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
</head>
<body>
    <div class="col-md-12">
      <div class="table-responsive">
        <table id="myTable" class="table table-bordered">
          <thead>
            <tr>
              <th><?php echo get_phrase('iniitials');?></th>
              <th><?php echo get_phrase('mpi');?></th>
              <th><?php echo get_phrase('time_in');?></th>
              <th><?php echo get_phrase('time_triaged');?></th>
              <th><?php echo get_phrase('time_seen');?></th>
              <th><?php echo get_phrase('ESI');?></th>
              <th><?php echo get_phrase('disposition');?></th>
              <th><?php echo get_phrase('time_out');?></th>

            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>



<script type="text/javascript">





$(document).ready(function() {
  // hide inputs for new patient
  $('#new_patient_data_inputs').hide();
  $('#chamber_inputs').hide();

  $('#add_chamber_button').click(function() {
    $('#chamber_inputs').show(200);
  });

  $('#myTable_length').hide();

  $('#myTable').DataTable( {
      "pageLength": 7,
      "processing": true,
      "serverSide": true,
      "searching":true,
      "ajax":{
       "url": "<?php echo base_url('index.php/admin/admittances/');?>",
       "dataType": "json",
       "type": "POST",
       "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
        },
        "columns": [
            { "data": "name" },
            { "data": "mpi" },
            { "data": "time_in" },
            { "data": "time_triaged" },
            { "data": "time_seen" },
            { "data": "esi" },
            { "data": "disposition" },
            { "data": "time_out" }
         ]

  } );/***end of my table init function*/


  $("#selUser").select2({
       ajax: {
         url: '<?= base_url() ?>index.php/admin/searchPatients',
         type: "post",
         dataType: 'json',
         data: function (params) {
            return {
            searchTerm: params.term // search term
            };
         },
         processResults: function (response) {
            return {
               results: response
            };
         },
         cache: true
       },
       minimumInputLength: 2,
   });
});

function hide_inputs() {
  $('#esi').val('');
  $('#disposition').val('');
  $('#chamber_inputs').hide(200);
}

function show_inputs() {
  $('#patient_type').val('new');
  $('#new_patient_data_inputs').fadeIn('slow');
  $('#old_patient_input').hide();
  $('#patientName').attr('required', true);
  $('#patientPhone').attr('required', true);
  $('#patientAddress').attr('required', true);
  $('#patientDoctor').attr('required', true);
}


</script>
</body>

</html>
