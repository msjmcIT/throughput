<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admittance_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function create_appointment()
	{
		$patient_type = $this->input->post('patient_type');
		$appointment_type = $this->input->post('appointment_type');
		$patient_mpi = $this->input->post('patient_mpi');

		if ($patient_type == 'new') {
			$patient_mpi = $this->input->post('mpi');
			$data_patient['mpi']  = $this->input->post('mpi');
			$data_patient['first_name']  = $this->input->post('first_name');
			$data_patient['last_name']  = $this->input->post('last_name');
			$data_patient['phone'] = $this->input->post('phone');
			$data_patient['phone2'] = $this->input->post('phone2');
			$data_patient['gender'] = $this->input->post('gender');
			$data_patient['date_of_birth'] = $this->input->post('date_of_birth');
			$data_patient['address'] = $this->input->post('address');
				$this->db->insert('patient', $data_patient);
				$patient_id = $this->db->insert_id();
		} else {
			$patient_mpi = $this->input->post('patient_mpi');
		}

		$data_appointment['patient_mpi'] = $patient_mpi;
		$data_appointment['time_in'] =  time();
		$data_appointment['esi_triage'] = $this->input->post('esi_triage');
		$data_appointment['disposition'] = $this->input->post('disposition');
		$this->db->insert('admittance', $data_appointment);

		return TRUE;
	}



	function manage_admittance($appointment_id){
		$patient_id = $this->db->get_where('admittance', array(
			'admittance_id' => $appointment_id
		))->row()->patient_mpi;

	  $triaged_time =  "";
		if (empty($this->input->post('esi_triage')) ) {
			$triaged_time =  "";
		}else{
			$triaged_time = time();
		}

		$seen_time = "";
		if (empty($this->input->post('visited_by_doctor')) ) {
			$seen_time =  "";
		}else{
			$seen_time =  time();
		}

		$out_time =  "";
		if (empty($this->input->post('disposition') === "discharged") ) {
			$out_time =  "";
		}else{
			$out_time =  time();
		}




		$data_appointment['time_seen'] = $seen_time;
		$data_appointment['time_out'] = $out_time;
		$data_appointment['time_triaged'] = $triaged_time;
    $data_appointment['visited_by_doctor'] = $this->input->post('visited_by_doctor');
		$data_appointment['patient_mpi'] = $patient_id;
		$data_appointment['esi_triage'] = $this->input->post('esi_triage');
		$data_appointment['disposition'] = $this->input->post('disposition');




		$this->db->where('admittance_id', $appointment_id);
		$this->db->update('admittance', $data_appointment);
		return TRUE;
	}


	function create_appointment_by_staff()
	{

		$patient_type = $this->input->post('patient_type');
		if ($patient_type == 'new') {
			$data_patient['name']  = $this->input->post('name');
			$data_patient['phone'] = $this->input->post('phone');
			if ($this->validation_model->validate_phone_number($data_patient['phone']) == FALSE) {
				return FALSE;
			} else {
				$this->db->insert('patient', $data_patient);
				$patient_id = $this->db->insert_id();
			}
		} else {
			$patient_id = $this->input->post('patient_id');
		}
		$data_appointment['timestamp']  = strtotime($this->input->post('timestamp'));
		$data_appointment['schedule']   = $this->input->post('schedule');
		$data_appointment['patient_id'] = $patient_id;
		$data_appointment['user_id']    = $this->session->userdata('login_user_id');
		$data_appointment['chamber_id'] = $this->session->userdata('chamber_id');
		$data_appointment['is_visited'] = 0;
		$data_appointment['appointment_notes'] = $this->input->post('notes');
		//$data_appointment['iuser'] = $this->session->userdata('login_user_id');
		$this->db->insert('appointment', $data_appointment);
		return TRUE;
	}

	function insert_appointment($data)
	{
		$this->db->insert('appointment', $data);
		//return $this->db->insert_id();
	}


	function get_appointment_by_day($timestamp, $clinic)
	{
		$query = $this->db->get_where('appointment', array(
			'timestamp' => $timestamp,
			'chamber_id' => $this->session->userdata('current_chamber')
		));
		return $query;
	}



	function delete_appointment($appointment_id)
	{
		$this->db->where('appointment_id', $appointment_id);
		$this->db->delete('appointment');
	}

	/*function create_or_get_prescription($appointment_id, $patient_id)
	{
		$query = $this->db->get_where('prescription', array(
			'appointment_id' => $appointment_id
		));

		if ($query->num_rows() <= 0) {
			$data['appointment_id'] = $appointment_id;
			$data['patient_id']     = $patient_id;
			$data['chamber_id']     = $this->session->userdata('chamber_id');
			$this->db->insert('prescription', $data);
			$prescription_id = $this->db->insert_id();

			$query = $this->db->get_where('prescription', array(
				'prescription_id' => $prescription_id
			));
		}
		return $query->row();
	}*/

	function get_chambers_by_account()
	{
		$query = $this->db->get('chamber');
		return $query;
	}

	function get_doctors_by_account()
	{
		$user_type = '1';
		$chamber_id = $this->session->userdata('current_chamber');
		$query = $this->db->get_where('staffusers', array(
			'chamber' => $chamber_id, 'user_type' => $user_type,
		));
		return $query;
	}

	function get_doctors_by_id($doc)
	{
		$query = $this->db->get_where('staffusers', array(
			'user_id' => $doc,
		));
		return $query;
	}

	function get_doctors()
	{
		$user_type = '1';
		$query = $this->db->get_where('staffusers', array(
			 'user_type' => $user_type,
		));
		return $query;
	}





	function allappointments_count()
    {
        $query = $this
                ->db
                ->get('admittance');

        return $query->num_rows();

    }

	function get_appointments($limit, $start)
  {
       $query = $this
                ->db
								->limit($limit, $start)
                ->get('admittance');
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

		function search_appointments($limit, $start, $search)
    {
        $query = $this
                ->db
								->like('patient_mpi',$search)
                ->limit($limit,$start)
                ->get('admittance');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }


		function search_appointments_count($limit, $start,$search)
		{
				$query = $this
								->db
								->like('patient_mpi',$search)
								->get('admittance');

				return $query->num_rows();
		}










}
