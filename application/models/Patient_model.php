<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class Patient_model extends CI_Model
{

	function __construct()
	{

		parent::__construct();
		$this->load->database();
	}

	function edit_patient($patient_id)
	{
		$data['first_name']    = $this->input->post('first_name');
		$data['last_name']    = $this->input->post('last_name');
		$data['date_of_birth']     = $this->input->post('date_of_birth');
		$data['phone']   = $this->input->post('phone');
		$data['phone2']   = $this->input->post('phone2');
		$data['gender']  = $this->input->post('gender');
		$data['address'] = $this->input->post('address');

		$medical_data                   = array();
		$medical_info['blood_group']    = $this->input->post('blood_group');
		$medical_info['height']         = $this->input->post('height');
		$medical_info['weight']         = $this->input->post('weight');
		$medical_info['blood_pressure'] = $this->input->post('blood_pressure');
		$medical_info['pulse']          = $this->input->post('pulse');
		$medical_info['respiration']    = $this->input->post('respiration');
		$medical_info['allergy']        = $this->input->post('allergy');
		$medical_info['diet']           = $this->input->post('diet');
		array_push($medical_data, $medical_info);

		$data['medical_info'] = json_encode($medical_data);

		// check if the mobile number is already in use by another user
		if ($this->validation_model->validate_phone_number($data['phone'], $patient_id) == FALSE) {
			return 'failed';
		} else {
			$this->db->where('patient_id', $patient_id);
			$this->db->update('patient', $data);
			return 'success';
		}
	}


	function get_patients_by_account()
	{
		//$this->db->limit(1000);
		$this->db->order_by('first_name', '	ASC');
		$query = $this->db->get('patient');

		return $query;
	}


	function allpatients_count()
    {
        $query = $this
                ->db
                ->get('patient');

        return $query->num_rows();

    }

	function get_patients($limit, $start)
  {
       $query = $this
                ->db
								->limit($limit, $start)
								->order_by('first_name', '	ASC')
                ->get('patient');

        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }

    }

		function search_patients($limit, $start, $search)
    {
        $query = $this
                ->db
								->like('mpi',$search)
                ->or_like('first_name',$search)
								->or_like('last_name',$search)
                ->limit($limit,$start)
                ->get('patient');


        if($query->num_rows()>0)
        {
            return $query->result();
        }
        else
        {
            return null;
        }
    }


		function searchPat($searchTerm=""){

     // Fetch users
     $this->db->select('*');
     $this->db->where("mpi like '%".$searchTerm."%' ");
     $fetched_records = $this->db->get('patient');
     $users = $fetched_records->result_array();

     // Initialize Array with fetched data
     $data = array();
     foreach($users as $user){
        $data[] = array("id"=>$user['mpi'], "text"=>$user['first_name'].' '.$user['last_name']);
     }
     return $data;
  }




		function search_patients_count($limit, $start,$search)
		{
				$query = $this
								->db
								->like('mpi',$search)
								->or_like('first_name',$search)
								->or_like('last_name',$search)
								->get('patient');

				return $query->num_rows();
		}












	function is_phone_exists($phone)
	{
		$query = $this->db->get_where('patient', array(
			'phone' => $phone
		));
		return $query->num_rows();
	}

	function insert_patient($data)
	{

		$this->db->insert('patient', $data);
		return $this->db->insert_id();
	}

	function get_patient_by_id($patient_id)
	{
		$query = $this->db->get_where('patient', array(
			'patient_id' => $patient_id
		));

		return $query->row();
	}

}
